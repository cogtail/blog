# TYPO3 CMS Extension "blog"

This blog extension uses TYPO3s core concepts and elements to provide a full-blown blog that users of TYPO3 can instantly understand and use.

## Requirements

### master
- TYPO3 CMS dev-master
- PHP 7.2

### v9.0
- TYPO3 CMS 9.5.1 or higher
- PHP 7.2

### v8.7
- TYPO3 CMS 8.7.0
- PHP 7.0

### v7.6
- TYPO3 CMS 7.6+
- PHP 5.5

## License
GPL-2.0-or-later

## Installation

* Use a `composer require t3g/blog`
* or download it from [bitbucket.typo3.com/scm/ext/blog](https://bitbucket.typo3.com/scm/ext/blog)
* or download ir from [TER](https://extensions.typo3.org/extension/blog/)

## Route configuration
The route enhancer config is not final yet, but a good start. You can simply include it in your site configuration:

    imports:
    - { resource: "EXT:blog/Configuration/Routes/Default.yaml" }

## Contribution

Any contributor is welcome to join our team. All you need is an account on our bitbucket system.
Please request a user account in our [service desk](https://jira.typo3.com/servicedesk/customer/portal/3/group/21). If you already have an account, please ask for access to the project in our [service desk](https://jira.typo3.com/servicedesk/customer/portal/3/group/21).

It is also highly recommended to join our [Slack Channel: #t3g-ext-blog](https://typo3.slack.com/archives/t3g-ext-blog)

